package com.example.nemanja.loaders.app_list_loader;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nemanja.loaders.R;

import java.util.List;

/**
 * A custom ArrayAdapter used by the {@link SecondActivity.AppListFragment} to display the
 * device's installed applications.
 */
public class AppListAdapter extends ArrayAdapter<AppEntry> {
    private final int mTagKey;
    private LayoutInflater mInflater;

    public AppListAdapter(Context ctx) {
        super(ctx, android.R.layout.simple_list_item_2);
        mInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mTagKey = R.layout.list_item_icon_text;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View view;

        if (convertView == null) {
            view = mInflater.inflate(R.layout.list_item_icon_text, parent, false);
        } else {
            view = convertView;
        }

        Object object = view.getTag(mTagKey);
        VH vh;
        if (object != null) {
            vh = VH.class.cast(object);
        } else {
            vh = new VH(view);
            view.setTag(mTagKey, vh);
        }

        AppEntry item = getItem(position);
        vh.iv.setImageDrawable(item.getIcon());
        vh.tv.setText(item.getLabel());

        return view;
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    public void setData(List<AppEntry> data) {
        clear();
        if (data != null) {
            addAll(data);
        }
    }

    static class VH {
        ImageView iv;
        TextView tv;

        VH(View v) {
            iv = (ImageView) v.findViewById(R.id.icon);
            tv = (TextView) v.findViewById(R.id.text);
        }
    }
}