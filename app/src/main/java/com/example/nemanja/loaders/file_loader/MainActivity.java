package com.example.nemanja.loaders.file_loader;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.nemanja.loaders.R;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<String> {
    TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportLoaderManager().initLoader(1, null, this);

        tv = (TextView) findViewById(R.id.tv1);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        return new FileLoader(this, "file1");
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {
        tv.setText(data);
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {

    }
}
