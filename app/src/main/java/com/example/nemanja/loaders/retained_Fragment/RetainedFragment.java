package com.example.nemanja.loaders.retained_Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

/**
 * Retained fragment (see onCreate below). This means it's not destroyed during configuration
 * change.
 * First time commiting it to fragment manager it will go through onAttach/onCreate ... etc
 * methods, on rotation it will go through onPause.../onDetach but will *skip* onDestroy.
 * When activity start being created again it will got through onAttach/onStart etc, but will NOT
 * go onCreate again.
 *
 * This is useful for holding cache for example or holding some operation that will survive
 * rotation.
 *
 * NOTE: When user clicks back or when system decide to kill parent Activity this fragment will
 * also be destroyed.
 *
 * All in all this is similar to loaders. If you need to query data from db for example just use
 * Loader.
 */
public class RetainedFragment extends Fragment {
    public static final String TAG = "RetainedFragment";
    private Callback mListener;

    public static RetainedFragment newInstance() {
        return new RetainedFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true); // NOTE this.
        Log.d(TAG, "onCreate: ");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, "onAttach: ");
        if (context instanceof Callback) {
            mListener = (Callback) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement Callback");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, "onDetach: ");
        mListener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: ");
    }

    public interface Callback {
        void onFragmentInteraction(Uri uri);
    }
}
