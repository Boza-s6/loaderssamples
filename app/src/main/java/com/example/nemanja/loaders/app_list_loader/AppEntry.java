package com.example.nemanja.loaders.app_list_loader;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;

import java.io.File;
import java.text.Collator;
import java.util.Comparator;

/**
 * This class holds the per-item data in our {@link AppListLoader}.
 */
public class AppEntry {
    /**
     * Performs alphabetical comparison of {@link AppEntry} objects.
     */
    public static final Comparator<AppEntry> ALPHA_COMPARATOR = new Comparator<AppEntry>() {
        Collator mCollator = Collator.getInstance();

        @Override
        public int compare(AppEntry object1, AppEntry object2) {
            return mCollator.compare(object1.getLabel(), object2.getLabel());
        }
    };

    private final PackageManager mPackageManager;
    private final Context mContext;
    private final ApplicationInfo mInfo;
    private final File mApkFile;
    private String mLabel;
    private Drawable mIcon;
    private boolean mMounted;

    public AppEntry(Context context, ApplicationInfo info) {
        mPackageManager = context.getPackageManager();
        mContext = context;
        mInfo = info;
        mApkFile = new File(info.sourceDir);
    }

    public ApplicationInfo getApplicationInfo() {
        return mInfo;
    }

    public long getId() {
        return ((long) mInfo.uid << 32) | (mInfo.sourceDir.hashCode() & 0x00000000ffffffffL);
    }

    public String getLabel() {
        return mLabel;
    }

    public Drawable getIcon() {
        if (mIcon == null) {
            if (mApkFile.exists()) {
                mIcon = mInfo.loadIcon(mPackageManager);
                return mIcon;
            } else {
                mMounted = false;
            }
        } else if (!mMounted) {
            // If the app wasn't mounted but is now mounted, reload its icon.
            if (mApkFile.exists()) {
                mMounted = true;
                mIcon = mInfo.loadIcon(mPackageManager);
                return mIcon;
            }
        } else {
            return mIcon;
        }

        return mContext.getDrawable(android.R.drawable.sym_def_app_icon);
    }

    @Override
    public String toString() {
        return mLabel;
    }

    void loadLabel() {
        if (mLabel == null || !mMounted) {
            if (!mApkFile.exists()) {
                mMounted = false;
                mLabel = mInfo.packageName;
            } else {
                mMounted = true;
                CharSequence label = mInfo.loadLabel(mPackageManager);
                mLabel = label != null ? label.toString() : mInfo.packageName;
            }
        }
    }
}