package com.example.nemanja.loaders.file_loader;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.AsyncTaskLoader;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;

/**
 * Created by nemanja on 6/20/16.
 */

public class FileLoader extends AsyncTaskLoader<String> {
    public static final String TAG = "TAG";
    private final Handler mHandler;
    private final String mPath;
    private FileObserver mFileObserver;
    private String mData;
    private final File mInputFile;

    public FileLoader(Context context, String path) {
        super(context);
        mHandler = new Handler(Looper.getMainLooper());
        mPath = path;
        mInputFile = new File(getContext().getExternalCacheDir(), mPath);
        try {
            mInputFile.createNewFile();
        } catch (IOException e) {
            throw new IllegalStateException();
        }
    }

    @Override
    public String loadInBackground() {
        try {
            Reader is = new InputStreamReader(new BufferedInputStream(new FileInputStream
                    (mInputFile)), StandardCharsets.UTF_8);

            StringBuilder builder = new StringBuilder();
            char[] buf = new char[1000];
            int count = 0;
            while ((count = is.read(buf, 0, 1000)) > 0) {
                builder.append(buf, 0, count);
            }
            is.close();
            return builder.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onStartLoading() {
        if (mData != null) {
            deliverResult(mData);
        }
        if (mFileObserver == null) {
            mFileObserver = new FileObserver(new File(getContext().getExternalCacheDir(), mPath)
                    .getPath());
            mFileObserver.startWatching();
        }
        if (takeContentChanged() || mData == null) {
            forceLoad();
        }
    }

    @Override
    protected void onReset() {
        super.onReset();
        onStopLoading();
        mFileObserver.stopWatching();
        mFileObserver = null;
        mData = null;
    }

    @Override
    public void deliverResult(String data) {
        if (isReset()) {
            return;
        }
        mData = data;
        if (isStarted()) {
            super.deliverResult(mData);
        }
    }

    private class FileObserver extends android.os.FileObserver {

        Runnable run = new Runnable() {
            @Override
            public void run() {
                onContentChanged();
            }
        };

        FileObserver(String path) {
            super(path, MODIFY|DELETE_SELF);
        }

        @Override
        public void onEvent(int event, String path) {
            mHandler.post(run);
        }
    }
}
